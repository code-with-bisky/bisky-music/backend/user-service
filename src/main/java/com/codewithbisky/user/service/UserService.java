package com.codewithbisky.user.service;

import com.codewithbisky.user.model.StorageProvider;
import com.codewithbisky.user.model.User;
import org.springframework.web.multipart.MultipartFile;

public interface UserService {

    User save(User user);
    User update(User user, String userId);
    User getById(String userId);
    void deleteUserById(String userId);
    void uploadProfilePicture(String userId, String key, MultipartFile file, StorageProvider storageProvider) throws Exception;
}
