package com.codewithbisky.user.mapper;

import com.codewithbisky.user.dto.UserDto;
import com.codewithbisky.user.model.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class DtoMapper {


    public abstract  User map(UserDto userDto);
    public abstract  UserDto map(User user);
}
